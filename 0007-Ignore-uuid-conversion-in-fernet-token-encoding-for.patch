From 69448008ef3db6cd59fd294b3b90936e2b1b87ea Mon Sep 17 00:00:00 2001
From: Ulrich Schwickerath <ulrich.schwickerath@gmail.com>
Date: Tue, 13 Dec 2022 20:40:33 +0100
Subject: [PATCH 07/13] Ignore-uuid-conversion-in-fernet-token-encoding-for

---
 keystone/conf/fernet_tokens.py     |  6 ++++++
 keystone/token/token_formatters.py | 23 +++++++++++++++++------
 2 files changed, 23 insertions(+), 6 deletions(-)

diff --git a/keystone/conf/fernet_tokens.py b/keystone/conf/fernet_tokens.py
index 728838c0f..395cad0fb 100644
--- a/keystone/conf/fernet_tokens.py
+++ b/keystone/conf/fernet_tokens.py
@@ -53,11 +53,17 @@ highest numerical index), and one secondary key (every other index). Increasing
 this value means that additional secondary keys will be kept in the rotation.
 """))
 
+ignore_project_id = cfg.BoolOpt(
+    'ignore_project_id',
+    default=False,
+    help=utils.fmt("Enables/disables the uuid conversion for project ids"))
+
 
 GROUP_NAME = __name__.split('.')[-1]
 ALL_OPTS = [
     key_repository,
     max_active_keys,
+    ignore_project_id,
 ]
 
 
diff --git a/keystone/token/token_formatters.py b/keystone/token/token_formatters.py
index 76220b0ef..418585562 100644
--- a/keystone/token/token_formatters.py
+++ b/keystone/token/token_formatters.py
@@ -310,7 +310,7 @@ class BasePayload(object):
         return ks_utils.isotime(time_object, subsecond=True)
 
     @classmethod
-    def attempt_convert_uuid_hex_to_bytes(cls, value):
+    def attempt_convert_uuid_hex_to_bytes(cls, value, ignore=False):
         """Attempt to convert value to bytes or return value.
 
         :param value: value to attempt to convert to bytes
@@ -318,6 +318,8 @@ class BasePayload(object):
                   stored as bytes and uuid value as bytes or the original value
 
         """
+        if ignore:
+            return (False, value)
         try:
             return (True, cls.convert_uuid_hex_to_bytes(value))
         except (ValueError, TypeError):
@@ -471,7 +473,9 @@ class ProjectScopedPayload(BasePayload):
                  app_cred_id):
         b_user_id = cls.attempt_convert_uuid_hex_to_bytes(user_id)
         methods = auth_plugins.convert_method_list_to_integer(methods)
-        b_project_id = cls.attempt_convert_uuid_hex_to_bytes(project_id)
+        b_project_id = cls.attempt_convert_uuid_hex_to_bytes(
+            project_id,
+            CONF.fernet_tokens.ignore_project_id)
         expires_at_int = cls._convert_time_string_to_float(expires_at)
         b_audit_ids = list(map(cls.random_urlsafe_str_to_bytes,
                            audit_ids))
@@ -510,7 +514,9 @@ class TrustScopedPayload(BasePayload):
                  app_cred_id):
         b_user_id = cls.attempt_convert_uuid_hex_to_bytes(user_id)
         methods = auth_plugins.convert_method_list_to_integer(methods)
-        b_project_id = cls.attempt_convert_uuid_hex_to_bytes(project_id)
+        b_project_id = cls.attempt_convert_uuid_hex_to_bytes(
+            project_id,
+            CONF.fernet_tokens.ignore_project_id)
         b_trust_id = cls.convert_uuid_hex_to_bytes(trust_id)
         expires_at_int = cls._convert_time_string_to_float(expires_at)
         b_audit_ids = list(map(cls.random_urlsafe_str_to_bytes,
@@ -606,7 +612,8 @@ class FederatedScopedPayload(FederatedUnscopedPayload):
         b_user_id = cls.attempt_convert_uuid_hex_to_bytes(user_id)
         methods = auth_plugins.convert_method_list_to_integer(methods)
         b_scope_id = cls.attempt_convert_uuid_hex_to_bytes(
-            project_id or domain_id)
+            project_id or domain_id,
+            CONF.fernet_tokens.ignore_project_id if project_id else False)
         b_group_ids = list(map(cls.pack_group_id, federated_group_ids))
         b_idp_id = cls.attempt_convert_uuid_hex_to_bytes(identity_provider_id)
         expires_at_int = cls._convert_time_string_to_float(expires_at)
@@ -664,7 +671,9 @@ class OauthScopedPayload(BasePayload):
                  app_cred_id):
         b_user_id = cls.attempt_convert_uuid_hex_to_bytes(user_id)
         methods = auth_plugins.convert_method_list_to_integer(methods)
-        b_project_id = cls.attempt_convert_uuid_hex_to_bytes(project_id)
+        b_project_id = cls.attempt_convert_uuid_hex_to_bytes(
+            project_id,
+            CONF.fernet_tokens.ignore_project_id)
         expires_at_int = cls._convert_time_string_to_float(expires_at)
         b_audit_ids = list(map(cls.random_urlsafe_str_to_bytes,
                            audit_ids))
@@ -746,7 +755,9 @@ class ApplicationCredentialScopedPayload(BasePayload):
                  app_cred_id):
         b_user_id = cls.attempt_convert_uuid_hex_to_bytes(user_id)
         methods = auth_plugins.convert_method_list_to_integer(methods)
-        b_project_id = cls.attempt_convert_uuid_hex_to_bytes(project_id)
+        b_project_id = cls.attempt_convert_uuid_hex_to_bytes(
+            project_id,
+            CONF.fernet_tokens.ignore_project_id)
         expires_at_int = cls._convert_time_string_to_float(expires_at)
         b_audit_ids = list(map(cls.random_urlsafe_str_to_bytes,
                            audit_ids))
-- 
2.41.0

